# Iridium

A diferent way to control a headless browser diferent to selenium

### Build dependencies
- [poetry](https://github.com/sdispater/poetry)

### System dependencies
- GNU/Linux with X.org (xvfb and a WM can works)
- xdotool
- xclip/xsel (xsel doesn't always seem to work)
- A web browser (firefox/chrome)

### Dependencies
- Python 3.x
- requests-html
- clipboard
- python-libxdo

### Installation
```
pip install iridium
```

### Build and install from source
```
poetry build
pip install dist/iridium*.whl --user
```

### Usage
**Generate a new driver**
```
In [1]: from iridium import Iridium
In [2]: driver = Iridium("firefox")
```
**Access to a website**
```
In [3]: driver.get("http://example.com/")
```
> http or https is not necesary

**get an element of the DOM by a CSS selector**
```
In [4]: elem = driver.get_element("a")
```

**Click to an element of the DOM**
```
In [5]: elem.click()
In [6]: # Or you can directly do the "click" method of the driver
In [7]: driver.click("h1")
```

**Get an attribute of an element**
```
In [8]: elem.get_attribute("href")
Out[8]: "https://www.iana.org/domains/example"
In [9]: elem.get_text()
Out[9]: "More information..."
```

**Check if an element exists**
```
In [8]: elem = driver.get_element("h1")
In [9]: elem.exists()
out[9]: True
```

**Execute javascript code**
```
In [11]: driver.execute_script('''
document.querySelector('body').style.backgroundColor = "red";
''')
```

**Get all the HTML content of the page**
```
In [12]: driver.page_source
Out[12]: "<!doctype html>\n<html>"...
```

### TO-DO
- [ ] Get the URL of the current page
